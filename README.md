# EasyUSD

The purpose of the EasyUSD module is to simplify the use of the Pixar USD Python API by creating an abstraction layer that is more human-readable, memorable and reduces the user’s exposure to complex code.

## Overview
I am using the EasyUSD module to not only simplify my life, but also improve my understanding of USD. I welcome input and thoughts, as I want this module to grow and become useful to others. I understand that EasyUSD adds a layer of abstraction which means we lose efficiency, however one of the goals is to simplify the experience for users which means sacrificing speed. If you are keen for speed consider exploring more of the Pixar Low Level API such as [PcP](https://openusd.org/release/api/pcp_page_front.html)/[Sdf](https://openusd.org/release/api/sdf_page_front.html) as these are far more performant.

This repo is very early days, and thus should be considered very much Work in Progress. There is still a lot to work on and I hope this repo grows to become useful.

Each function has documentation, which can be explored in the [function docs](https://gitlab.com/ChrisC_Git/easyusd/-/blob/main/docs/easyusd.md).

The Nvidia Omniverse [Python API](https://docs.omniverse.nvidia.com/kit/docs/omni.usd/latest/omni.usd.html) also has some useful functions, such as: duplicate_prim, etc

## Whats the point
- Simplicity, easy to install, easy to understand.
- New comers to USD will have easier access to tools, methods & techniques
- Faster R&D of tools before removing any unnecessary abstraction and putting the tools into Production.

## Useful USD Information
- stage.Save() method will maintain the referenced assets and dependencies
- the stage.Export(output_file) method in USD will flatten the file, meaning it will include all the referenced assets and dependencies directly within the exported file. If you want to save references to a new file EasyUSD has a save_as() function that can help.

## Useful USD Functions & Methods
### stage level
- stage.GetPrimAtPath()
### prim level
- prim.GetPath()
- prim.GetName()
- prim.GetParent()
- prim.GetChildren()
- prim.GetChildrenNames()

## Virtual Environment
I work in a virtual environment where I install `usd-core`. To do the same run:
- `python -m venv venv`
-  Win: `venv\Scripts\activate` or Linux: `source ./venv/Scripts/activate`
- `pip install usd-core`

## To Do:
There is lots, but here is just a list of things I have done & would still like to do.
- [ ] Get / Set Attribs
- [ ] Create Camera
- [ ] Variants
    - [ ] Add Variant
    - [x] Set Variant
    - [x] Get Variants
- [ ] Modify the Hierarchy - see WIP: modify_hierarchy() & brute_rename()
- [ ] The ability to set a default prim, even if one exists.
- [ ] Set Subdiv Scheme
- [ ] remove_empty_parents - Similar to remove prim recursive, but only empty prims
- [x] Create USD Preview Materials
    - [ ] Ability to connect texture files.
- [x] Get frame count(timeSamples) from USD
- [x] Create Overs
- [x] Set Prim Type
    - [ ] Check if a type exists and not over write it
- [x] Extract a prim to file
    - [x] Specify a new output_prim_path
- [x] Get Extents
- [x] Rename Prim
- [x] Remove prims
    - [x] Recursively (this can be dangerous)
- [x] Position a prim Translate, Rotate, Scale
- [x] Set Purpose
- [x] Set Extents
- [x] Set Visibility
- [x] Create Prim
- [x] Create Class Prim
- [x] Set Asset Info
- [x] Add Inherit
- [x] Add a payload
- [x] Experimental: Get Payloads
- [x] Experimental: Brute Rename

## Thanks
- The CustomUVChecker_byValle_1K created on [uvchecker.vinzi.xyz/](https://uvchecker.vinzi.xyz/)
- The [pixar glossary](https://openusd.org/release/glossary.html)
- Learnt a lot from the [USD Cookbook](https://github.com/ColinKennedy/USD-Cookbook)
- Great information at [Nvidia Omniverse Docs](https://docs.omniverse.nvidia.com/prod_usd/prod_kit/programmer_ref/usd.html)
- [USD Survival Guide](https://lucascheller.github.io/VFX-UsdSurvivalGuide/index.html)
- [Book of USD](https://remedy-entertainment.github.io/USDBook/usd_primer.html)