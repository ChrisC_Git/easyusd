# Easyusd Module
This module imports: pxr

== EasyUSD 0.0.10 ==

The purpose of the EasyUSD module is to simplify the use of the
Pixar USD Python API by creating an extra layer of abstraction
that is more human-readable, memorable and reduces the users
exposure to complex code paths.

Requires Python 3 or above as well as USD-Core
- pip install usd-core

## main
Script entry point

## get_prim_and_path
Converts a string prim_path to an Sdf.Path, if necessary, and retrieves the corresponding prim from the USD stage.

Using Sdf.Path is similar to 'sys.path' in that you can 'AppendChild(), GetParentPath(), AppendVariantSelection(), etc

- [OpenUSD.org API Docs](https://openusd.org/release/api/class_sdf_path.html#sec_SdfPath_Overview)
- [USD Survival Guide](https://lucascheller.github.io/VFX-UsdSurvivalGuide/core/elements/path.html)

Args:
- stage (Usd.Stage):                  The USD stage from which to get the prim.
- prim_path (str, Sdf.Path, Usd.Prim): The path to the prim. If it's a string, it will be converted to an Sdf.Path.
                                         If it's a Usd.Prim, its path will be used.

Returns:
- tuple:                               A tuple containing the prim (Usd.Prim) and the prim_path (Sdf.Path).

Example Usage:
- prim, _ = get_prim_and_path(stage, "/bicycle")
- prim = get_prim_and_path(stage, "/bicycle")[0]

## create_stage
Create an empty stage, with a prim xform if specified. This function
will remove a trailing '/' from the path such as on '/world/xform/'

Args:
- path:       needs to include directory and filename
- prim (str): the address of primitive such as '/cube_model'

Returns:
- stage:      an opened stage file via Usd.Stage.Open()

## prim_exists
Checks if a prim exists at the prim path given.

Args:
- stage:                          an opened stage file via Usd.Stage.Open()
- prim (str, Sdf.Path, Usd.Prim): the address to the primitive such as '/cube_model'
                                    or the prim object itself
- verbose (bool):                 simply prints out if the prim is valid or not

Returns:
- bool: True if the prim exists, False if it does not.

## create_prim
Create a simple prim of name (last position in path will be prim name)
it also sets the prim type (recursively if True)

Some common types are: "Xform", "Mesh", "Scope", "Camera", etc

Args:
- stage:                  an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim): the address to the primitive such as '/cube_model'
                            or the prim object itself
- prim_type (str):        specify the primitive type
- recursive (bool):       True - Will set all prim Types | False sets last

Returns:
- bool: The newly created prim

## create_over
Create an over at the prim_path specified
An over is used when you want to add data to an existing
hierarchy without defining a new prim.

Prims defined with 'over' are only loaded if the prim in another
layer has been specified with a 'def'.

Args:
- stage:                  an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim): the prim path to the primitive/s such as '/Set/Set_Name'
- recursive (bool):       when true sets the kind for all prims in path.

Returns:
- None

## create_class_prim
Create a class such as:

class Xform "__class__"{}

Args:

Returns:
- prim: the newly created ClassPrim

## get_sublayers
Get the sublayers of a usd file

Args:
- stage: an opened stage file via Usd.Stage.Open()

Returns:
- sublayers (list): the sublayers of a usd file

## set_sublayers
Sublayer is just a Union of Two or More Files, simple A + B.
You could loop this for multiple files

Args:
- stage:     An opened stage file via Usd.Stage.Open()
- sublayer:  The usd file you want to set as the sub layer
- rel_paths: When true, the path will be set to relative

Returns:
- None

## insert_sublayer
Sublayer is just a Union of Two or More Files, simple A + B.
This function lets you insert a new sublayer at any position
inside a usd file already containing sublayers.

Useful when adding animation to an usd already containing
model & lookdev, as the prefered order is model, anim, lookdev

Args:
- stage:     An opened stage file via Usd.Stage.Open()
- sublayer:  The usd file you want to set as the sublayer
- position:  Where in the sublayer stack do you want this file
- rel_paths: When true, the path will be set to relative

Returns:
- None

## set_metadata
Set stage metadata

Args:
- stage:          an opened stage file via Usd.Stage.Open()
- fps (int):      frame rate per second (24 | 25)
- timecode (int): time code per second (24 | 25)
- units (str):    'meters' or 'centimeters'
- upaxis (str):   the UpAxis of the stage (Maya/Houdini = 'Y' | Blender = 'Z')

Returns:
- None

## set_reference
Allows you to reference in a file and graft it into the scene graph.
You could loop this for multiple files

Creates a reference with the xform prims required, this does not create
an over.

Args:
- stage:            an opened stage file via Usd.Stage.Open()
- reference:        the usd file you want to set as the reference
- destination_path: where in the stage hierarchy to place the reference
- prim_path:        specify the position of the prim you want to reference, otherwise the default prim will be used
                      call set_default_prim(usdfile) to set the default before running set_reference()
- rel_paths:        When true, the path will be set to relative

Returns:
- None

## add_payload
Place a payload into a stage. A payload is a reference with a weaker opinion.

Args:
- stage (Usd.Stage):    An opened stage file via Usd.Stage.Open()
- payload_path (str):   the path to the payload usd file
- dest_prim_path (str): Prim path for payload
- prim_path (str):      specify the position of the prim inside the payload usd file,
                          otherwise the default prim will be used.
                          Call add_default_prim(usdfile) to set the default before running add_payload()

## save_as
USD doesnt have native .SaveAs() method so this just takes a modified stage
and transfers the data to a new file allowing you to save it elsewhere.

Args:
- stage:             An opened stage file via Usd.Stage.Open()
- output_file (str): Path to new save file.

Returns:
- None

## set_asset_info
Take an asset_info dictionary and adds it to the usd root_layer

asset_info = {"identifier": "@../path/to/usdfile.usd@",
              "name": "AssetName",
              "version": "003"}

Args:
- stage:             an opened stage file via Usd.Stage.Open()
- asset_info (dict): example above

Returns:
- None

## get_default_prim
Get the default prim of the usd file

Args:
- stage: an opened stage file via Usd.Stage.Open()

Returns:
- Default Prim

## set_default_prim
Adds the default prim assignment to the specified file.

Args:
- stage:      an opened stage file via Usd.Stage.Open()
- prim_path:  the internal hierarchy path if you know it, if None it sets the top level prim path

Returns:
- Default Prim

## set_prim_kind
Set the prim at the specified path to the specified kind.

- "group": Indicates that the prim is a group or collection of other prims.
- "component": Indicates that the prim is a component or part of a larger assembly.
- "assembly": Indicates that the prim is an assembly or a collection of components.
- "subcomponent": Indicates that the prim is a subcomponent or subset of a larger component.

Args:
- stage:            an opened stage file via Usd.Stage.Open()
- prim_path (str):  the prim path to the primitive such as '/Set/Set_Name'
- kind (str):       the kind as definded above
- recursive (bool): when true sets the kind for all prims in path.

Returns:
- None

## set_prim_type
Set the prim at the specified path to the specified type.

Some common types are: "Xform", "Mesh", "Scope", "Camera", etc

TODO: check if a type exists and not over write it

Args:
- stage:            an opened stage file via Usd.Stage.Open()
- prim_path (str):  the prim path to the primitive such as '/Set/Set_Name'
- prim_type (str):  the type as definded above
- recursive (bool): when true sets the kind for all prims in path.

Returns:
- None

## set_prim_name
Renames a primitive. This only works on the give prim or the
last prim in the prim path.

NOTE: This function has been replaced by rename_prim() but lives here
      as an example of how to use Sdf.BatchNamespaceEdit()

Args:
- prim (str or obj): the address to the primitive such as '/set/setname'
                       or the prim object itself
- name (str):        only the new name for the primitive. Not the whole path.

Returns:
- None

## rename_prim
Renames a primitive. This only works on the give prim or the
last prim in the prim path.

Args:
- prim (str or obj): the address to the primitive such as '/set/setname'
                       or the prim object itself
- name (str):        only the new name for the primitive. Not the whole path.

Returns:
- None

## get_prim_type
Gets the type, if any, of the given primitive

Some common types are: "Xform", "Mesh", "Scope", "Camera", etc

Args:
- stage:                   an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim):  the address to the primitive such as '/Set/Set_Name'

Returns:
- prim_type or None

## get_prim_kind
gets the kind, if any, of the given primitive

this is identical to just calling prim.GetMetadata('kind')
its just wrapped in a function

Args:
- stage:                   an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim):  the address to the primitive such as '/Set/Set_Name'

Returns:
- prim_kind or None

## set_purpose
Set the purpose of a prim. Commonly this is 'proxy', 'render' & 'guide', 'default_'.

Args:
- stage:                       an opened stage file via Usd.Stage.Open()
- prim_name (str or Usd.Prim): the address to the primitive such as '/cube_model'
                                 or the prim object itself
- new_name (str):              new name for the primitive

Returns:
- bool: True if the prim was successfully removed, False otherwise

## set_relationship
Give a prim a relationship to another prim target.

Commonly this is 'simproxy', 'proxyPrim', 'material:binding', 'geometry:subset'.

- [Pixar Relationship Docs](https://openusd.org/release/glossary.html#usdglossary-relationship)
- [OpenUSD.org API Docs](https://openusd.org/dev/api/class_usd_relationship.html)

Args:
- stage:                         an opened stage file via Usd.Stage.Open()
- prim_name (str or Usd.Prim):   the prim path to the primitive such as '/cube_model'
                                   or the prim object itself
- target_prim (str or Usd.Prim): path to relationship target
- target_relationship (str):     'simproxy', 'proxyPrim', etc

Returns:
- bool: True if the prim was successfully removed, False otherwise

## set_extent
Calculates & sets the extentsHint of a prim based on its World Bounds

Args:
- stage:                     an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim):    the prim path to the primitive such as '/root/cube_model'
                               or the prim object itself

Returns:
- extents_hint (Vec3fArray): two-by-three array containing the endpoints of the
                               axis-aligned, object-space extent 

## get_extent
Gets the extentsHint of a prim

[Pixar Inspect Tutorial](https://openusd.org/release/tut_inspect_and_author_props.html)

Args:
- stage:                      an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim):     the address to the primitive such as '/root/cube_model'
                                or the prim object itself

Returns:
- extents_hint (Vec3fArray): two-by-three array containing the endpoints of the
                               axis-aligned, object-space extent

## set_prim_visibility
Set the prim visibility attribute

Args:
- stage:                  an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim): the prim path to the primitive such as '/root/cube_model'
                            or the prim object itself
- visibility (bool):      True = Visible, False = Invisible                                    

Returns:
- None  

## add_inherit
Allows you to add an inhert class to an existing usd prim.

An inherit enables a prim to contain information that the base prim inherits.
[Pixar Inherit Docs](https://openusd.org/release/glossary.html#usdglossary-inherits)

Args:
- stage:                   an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim):  the prim path to the primitive such as '/cube_model'
                             or the prim object itself
- inherit_class (Usd.Prim): the prim of the class you wish to inherit from

## extract_prim_to_file
Extracts the given prim at the prim_path out to its own usd file
If output_prim_path is specified it will repath the prim from the source
to a new prim_path, otherwise it maintains the original path.

Thanks to [USD Survival Guide](https://lucascheller.github.io/VFX-UsdSurvivalGuide/production/concepts.html#sdfCopySpec)

Args:
- stage:                  an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim): the address to the primitive such as '/cube_model'
                            or the prim object itself
- output_file (str):      the destination location for the .usd
- output_prim_path (str): the path to a new prim output path '/asset/cube_model'

Returns:
- None

## get_point_positions
Get the point positions of an object in a stage

Args:
- stage:                  an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim): the address to the primitive such as '/root/cube_model'
                            or the prim object itself

Returns:
- point_positions (point3f[]): the point positions

## set_point_positions
Sets the point positions of the prim object.

Documentation: Two methods are available, we have implemented the first method
### Method 01: for simply giving the new positions
```
new_vertex_positions = [(-5, 0, -5), (0, 0, -5), (5, 0, -5), (-5, 0, 0)]
points_attribute.Set(new_vertex_positions)
```

### Method 02: Shows we can modify the positions based on index
```
for i, vertex_position in enumerate(current_vertex_positions):
- # Example: Multiply each coordinate by 2
- new_position = vertex_position * 2.0
- current_vertex_positions[i] = new_position
points_attribute.Set(current_vertex_positions)
```

Args:
- stage:                       an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim):      the address to the primitive such as '/root/cube_model'
                                 or the prim object itself
- point_positions (point3f[]): a single list containing multiple Gf.Vec3d tuples per point
                                 eg. [(-5, 0, -5), (0, 0, -5), (5, 0, -5), (-5, 0, 0)]

Returns:
- None    

## create_geomsubset
Creates a geomsubset that includes the given vertices or points
from a list, eg: [0, 1, 2, 3, 4, 5]

Note: A geomsubset does not allow you to author visibility for it or override its purpose

Args:
- stage:                  an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim): the address to the primitive such as '/cube_model'
                            or the prim object itself
- subset_name (str):      the name for your subset
- points (list):          a list containing your point numbers

Returns:
- None

## set_variant
This function sets the variant on the specified prim.
Variants live within a Variant Set, so both are required.

Args:
- stage:                  an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim): the address to the primitive such as '/cube_model'
- variant_set (str):      the variant set name
- variant_name (str):     the variant name

Returns:
- None

## create_variant_file
Creates a new usd file containing the given usd files as references under a single
new prim where each file is a new variant. The variant name is pulled from the
usd file itself.

Args:
- stage:                an opened stage file via Usd.Stage.Open()
- prim_path (str):      the name of the top level asset name
- variant_set (str):    the variant set name
- variant_files (list): A list of the paths including filenames of the variant files
- rel_paths: When true, the path will be set to relative

## get_variants
This function returns a dict of the variant sets & variants of the
specified prim.

Args:
- stage:                       an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim):      the address to the primitive such as '/cube_model'

Returns:
- variant_ouput (dict[list]): a dictionary with a list of each variant

## get_hierarchy
Get the hierarchy of the usd file

Args:
- stage: an opened stage file via Usd.Stage.Open()

Returns:
- The stage hierarchy

## remove_prim
Remove the specified prim if it exists

Args:
- stage:            an opened stage file via Usd.Stage.Open()
- prim_path (str):  the address to the primitive such as '/set/setname'
- recursive (bool): If True will recursively remove all prims in the path

Returns:
- bool: True if the prim was successfully removed, False otherwise

## find_prim_by_name
Returns the prim by searching the stage for matching prim_name

Args:
- stage:           an opened stage file via Usd.Stage.Open()
- prim_name (str): the address to the primitive such as 'cube_model'

Returns:
- prim (Usd.Prim): the prim at its path in the stage.

## modify_hierarchy
WIP Function to modify the hierarchy of a stage.

Args:
- stage:                  an opened stage file via Usd.Stage.Open()
- current_primpath (str): the prim path you wish to modify
- new_primpath (str):     the resulting prim path
- remove (bool):          True will remove the current_primpath from the stage

Returns:
- None

## prim_is_xform
Check to see if a prim is an xform.

Args:
- prim (str): a prim path for example inside a loop 'for prim in stage.Traverse():'

Returns:
- True if prim is xform

## prim_is_geometry
Check to see if a prim contains Geometry.
Mesh prims in USD are typically wrapped in a UsdGeom.Mesh schema, which inherits from UsdGeom.Geometry.

Args:
- prim (str): a prim path for example inside a loop 'for prim in stage.Traverse():'

Returns:
- True if prim contains Geometry

## prim_is_material
Check to see if a prim contains Material.=

Args:
- prim (str): a prim path for example inside a loop 'for prim in stage.Traverse():'

Returns:
- True if prim contains Material

## prim_isA
A wrapper for the `prim.IsA()`

Supported prim types are: "Xform", "Mesh", "Geo", "Material"
TODO: "Scope", "Camera"

Args:
- prim (str):      a prim path for example inside a loop 'for prim in stage.Traverse():'
- prim_type (str): string containing type you want to test for

Returns:
- True if prim matches prim_type

## set_sca_rot_tran
Set the Scale, Rotation and Translation (SRT) of the specified prim.

Args:
- stage:             an opened stage file via Usd.Stage.Open()
- prim_path (str):   the address to the primitive such as '/Set/Set_Name'
- scale (list):       the [x, y, z] scale for the primitive
- rotation (list):    the [x, y, z] rotation for the primitive
- translation (list): the [x, y, z] coordinates for the primitive

Returns:
- None

## create_light
Create a light in stage at specified prim path

Lights
- "DiskLight":    Creates a Spot Light in Houdini, supports radius, cone_angle & cone_softness values.
- "SphereLight":  Creates a Point Light in Houdini although Blender enables cone_angle making this act like a spot.
- "DistantLight": Creates a Distant Light in Houdini or Sun Light in Blender
- "RectLight":    Creates a Area Light, supports a texture file
- "DomeLight":    Creates a Dome Light in Houdini which is similar to Environment Light, supports a texture file
                  Blender 4.0.0 Alpha exports its Environment Light to '/lights/environment' as a DomeLight

Args:
- stage:                 an opened stage file via Usd.Stage.Open()
- prim_path (str):       the address to the primitive such as '/lights/LightName'
- color (3fArray):       the color as a list [R, G, B]
- intensity (float):     the intensity
- exposure (float):      the exposure
- specular (float):      the specular
- radius (float):        DiskLight Only: the radius of the light
- cone_angle (float):    DiskLight Only: the frustum angle of the light
- cone_softness (float): DiskLight Only: the edge of the cone softeness value
- texture_file (str):    RectLight & DomeLight: the path and file name to texture file
- texture_width (float): RectLight Only: the texture width

Returns:
- None

## create_usdpreviewsurface
Creates a simple usd preview material with the specified values.
Currently it does not support image texture files.

[UsdPreviewSurface Specification](https://openusd.org/release/spec_usdpreviewsurface.html)

Args:
- stage:                      an opened stage file via Usd.Stage.Open()
- material_path (str):        the scene path to store the material, commonly /mtl
- diffuseColor (3fArray):     the color as a tuple (R, G, B)
- emissiveColor (3fArray):    the color as a tuple (R, G, B)
- roughness (float):          the roughness
- specular (float):           the specular
- opacity (float):            the opacity, 1.0 is fully opaque
- opacityThreshold (float):   the opacityThreshold input is useful for creating geometric cut-outs based on the opacity input
- metallic (float):           the metallic value
- ior (str):                  the Index of Refraction, used on translucent objects
- clearcoat (float):          the second specular
- clearcoatRoughness (float): the roughness for the second specular

Returns:
- None

## get_frame_count
Returns the number of frames for a prim in a usd file.

Args:
- stage:                  an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim): the prim path to the primitive/s such as '/Set/Set_Name'

Returns:
- frame_count (int):      the total number of timeSamples on the prim

## brute_rename
Making sdf layer namespace edits can be rather challenging. This allows a more
'brute force' method of renaming a prim. Its a simple text search & replace
which does possibly slow it down and of course does not conform to USD norms.

Here be Dragons!!

Args:
- stage:           an opened stage file via Usd.Stage.Open()
- prim_name (str): the address to the primitive such as 'cube_model'
                     or the prim object itself
- new_name (str):  new name for the primitive

## get_payloads
gets the file path, if any, of the given primitive
if prim is set to None it will return a list of all payloads
found and on what prim they are found:

payloads = [prim_name, payload]

Args:
- stage:                   an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim):  the address to the primitive such as '/Set/Set_Name'

Returns:
- payload (file, None or list)

## add_variant
Work in Progress

This function adds variants to a known variant set

Args:
- stage:                  an opened stage file via Usd.Stage.Open()
- prim (str or Usd.Prim): the address to the primitive such as '/cube_model'
- variant_set (str):      the variant set name
- variant_name (str):     the variant name

Returns:
- None

