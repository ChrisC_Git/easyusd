# region INFO
"""
== EasyUSD 0.0.10 ==

The purpose of the EasyUSD module is to simplify the use of the
Pixar USD Python API by creating an extra layer of abstraction
that is more human-readable, memorable and reduces the users
exposure to complex code paths.

Requires Python 3 or above as well as USD-Core
- pip install usd-core
"""
# endregion

# region IMPORTS
from pxr import Usd, UsdGeom, Sdf, Gf, UsdShade, Vt, Ar
import os, re

# endregion

# region ENTRY POINT
def main():
    """
    Script entry point
    """
    print("this script should be imported not run directly")


# endregion

# region HELPER FUNCTIONS
def get_prim_and_path(stage, prim_path):
    '''
    Converts a string prim_path to an Sdf.Path, if necessary, and retrieves the corresponding prim from the USD stage.

    Using Sdf.Path is similar to 'sys.path' in that you can 'AppendChild(), GetParentPath(), AppendVariantSelection(), etc

    - [OpenUSD.org API Docs](https://openusd.org/release/api/class_sdf_path.html#sec_SdfPath_Overview)
    - [USD Survival Guide](https://lucascheller.github.io/VFX-UsdSurvivalGuide/core/elements/path.html)
    
    Args:
        stage (Usd.Stage):                  The USD stage from which to get the prim.
        prim_path (str, Sdf.Path, Usd.Prim): The path to the prim. If it's a string, it will be converted to an Sdf.Path.
                                             If it's a Usd.Prim, its path will be used.

    Returns:
        tuple:                               A tuple containing the prim (Usd.Prim) and the prim_path (Sdf.Path).

    Example Usage:
        prim, _ = get_prim_and_path(stage, "/bicycle")
        prim = get_prim_and_path(stage, "/bicycle")[0]
    '''
    if isinstance(prim_path, str):
        if prim_path.endswith("/"):
            print("Warning: The input path ends with a '/'. It will be removed.")
            prim_path = prim_path.rstrip("/")
        prim_path = Sdf.Path(prim_path)
    elif isinstance(prim_path, Usd.Prim):
        prim = prim_path
        prim_path = prim.GetPath()
        return prim, prim_path
    elif not isinstance(prim_path, Sdf.Path):
        raise TypeError("prim_path must be a string, Sdf.Path, or Usd.Prim")

    prim = stage.GetPrimAtPath(prim_path)
    return prim, prim_path

# endregion

# region STAGES
def create_stage(path, prim=None):
    """
    Create an empty stage, with a prim xform if specified. This function
    will remove a trailing '/' from the path such as on '/world/xform/'

    Args:
        path:       needs to include directory and filename
        prim (str): the address of primitive such as '/cube_model'

    Returns:
        stage:      an opened stage file via Usd.Stage.Open()
    """
    stage = Usd.Stage.CreateNew(path)
    
    if prim:
        # check if prim is str
        if isinstance(prim, str):
            prim_path = prim.rstrip("/")
            xformPrim = UsdGeom.Xform.Define(stage, prim_path)
        else:
            print('Please specify prim path (str) eg. /world/xform')

    stage.GetRootLayer().Save()

    return stage

def prim_exists(stage, prim, verbose=None):
    """
    Checks if a prim exists at the prim path given.

    Args:
        stage:                          an opened stage file via Usd.Stage.Open()
        prim (str, Sdf.Path, Usd.Prim): the address to the primitive such as '/cube_model'
                                        or the prim object itself
        verbose (bool):                 simply prints out if the prim is valid or not

    Returns:
        bool: True if the prim exists, False if it does not.
    """
    prim_path = get_prim_and_path(stage, prim)[1]
    prim = stage.GetPrimAtPath(prim_path)
    if verbose and prim.IsValid():
        print(f'The prim {prim} exists')
    
    return prim.IsValid()

def create_prim(stage, prim, prim_type=None, recursive=False):
    """
    Create a simple prim of name (last position in path will be prim name)
    it also sets the prim type (recursively if True)

    Some common types are: "Xform", "Mesh", "Scope", "Camera", etc

    Args:
        stage:                  an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim): the address to the primitive such as '/cube_model'
                                or the prim object itself
        prim_type (str):        specify the primitive type
        recursive (bool):       True - Will set all prim Types | False sets last

    Returns:
        bool: The newly created prim
    """
    # convert prim_path string to prim
    if isinstance(prim, str):
        prim_path = prim.rstrip("/")
        prim = stage.GetPrimAtPath(prim_path)

    if recursive:
        prim_names = prim_path.split("/")

        current_path = ""
        for prim in prim_names:
            if prim:
                current_path = current_path + f"/{prim}"

                # Define a new prim
                prim = stage.DefinePrim(current_path, prim_type.capitalize())
    else:
        if prim_type is None:
            prim = stage.DefinePrim(prim_path)
        else:
            prim = stage.DefinePrim(prim_path, prim_type.capitalize())

    return prim

def create_over(stage, prim, recursive=None):
    """ 
    Create an over at the prim_path specified
    An over is used when you want to add data to an existing
    hierarchy without defining a new prim.

    Prims defined with 'over' are only loaded if the prim in another
    layer has been specified with a 'def'.

    Args:
        stage:                  an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim): the prim path to the primitive/s such as '/Set/Set_Name'
        recursive (bool):       when true sets the kind for all prims in path.

    Returns:
        None
    """
    # convert prim to prim_path (str)
    if not isinstance(prim, str):
        prim_path = str(prim.GetPath())
    else:
        prim_path = prim.rstrip("/")

    if recursive:
        prim_names = prim_path.split("/")
        current_path = ""

        #create whole prim path first
        prim = stage.DefinePrim(prim_path, "")
        # loop through all prims setting them to over
        for prim in prim_names:
            if prim:
                current_path += f"/{prim}"
                prim = stage.GetPrimAtPath(current_path)
                prim.SetSpecifier(Sdf.SpecifierOver)
    else:
        # Define the prim with the 'over' specifier
        prim = stage.DefinePrim(prim_path, "")
        prim.SetSpecifier(Sdf.SpecifierOver)

def create_class_prim(stage, prim_path, prim_type, recursive=False):
    """
    Create a class such as:

    class Xform "__class__"{}

    Args:

    Returns:
        prim: the newly created ClassPrim
    """
    if recursive:
        prim_names = prim_path.split("/")

    current_path = ""
    for prim in prim_names:
        if prim:
            current_path = current_path + f"/{prim}"

            # Define a new class prim
            prim = stage.CreateClassPrim(current_path)
            prim.SetTypeName(prim_type)
    
    return prim

def get_sublayers(stage):
    """
    Get the sublayers of a usd file

    Args:
        stage: an opened stage file via Usd.Stage.Open()

    Returns:
        sublayers (list): the sublayers of a usd file
    """
    root_layer = stage.GetRootLayer()
    sublayers = root_layer.subLayerPaths
    return sublayers

def set_sublayers(stage, sublayer, rel_paths=None):
    """
    Sublayer is just a Union of Two or More Files, simple A + B.
    You could loop this for multiple files

    Args:
        stage:     An opened stage file via Usd.Stage.Open()
        sublayer:  The usd file you want to set as the sub layer
        rel_paths: When true, the path will be set to relative

    Returns:
        None
    """
    if rel_paths:
        # python rel paths are ..\ but usd want .\ so we strip [1:]
        stage_file_path = stage.GetRootLayer().realPath
        sublayer = os.path.relpath(sublayer, stage_file_path)[1:]

    # get the root layer
    rootLayer = stage.GetRootLayer()

    # Set the sublayer
    rootLayer.subLayerPaths.append(sublayer)

def insert_sublayer(stage, sublayer, position, rel_paths=None):
    """
    Sublayer is just a Union of Two or More Files, simple A + B.
    This function lets you insert a new sublayer at any position
    inside a usd file already containing sublayers.

    Useful when adding animation to an usd already containing
    model & lookdev, as the prefered order is model, anim, lookdev

    Args:
        stage:     An opened stage file via Usd.Stage.Open()
        sublayer:  The usd file you want to set as the sublayer
        position:  Where in the sublayer stack do you want this file
        rel_paths: When true, the path will be set to relative

    Returns:
        None
    """
    if rel_paths:
        # python rel paths are ..\ but usd want .\ so we strip [1:]
        stage_file_path = stage.GetRootLayer().realPath
        sublayer = os.path.relpath(sublayer, stage_file_path)[1:]

    if not isinstance(position, int):
        raise Exception("Please specify position as integer!")

    # get the root layer
    rootLayer = stage.GetRootLayer()

    # Set the sublayer
    rootLayer.subLayerPaths.insert(position, sublayer)

def set_metadata(stage, fps=None, timecode=None, units=None, upaxis=None):
    """
    Set stage metadata

    Args:
        stage:          an opened stage file via Usd.Stage.Open()
        fps (int):      frame rate per second (24 | 25)
        timecode (int): time code per second (24 | 25)
        units (str):    'meters' or 'centimeters'
        upaxis (str):   the UpAxis of the stage (Maya/Houdini = 'Y' | Blender = 'Z')

    Returns:
        None
    """
    if fps:
        stage.SetFramesPerSecond(fps)
    if timecode:
        stage.SetTimeCodesPerSecond(timecode)

    if units == "meters" or units == "m":
        UsdGeom.SetStageMetersPerUnit(stage, UsdGeom.LinearUnits.meters)
    if units == "centimeters" or units == "cm":
        UsdGeom.SetStageMetersPerUnit(stage, UsdGeom.LinearUnits.centimeters)
    if upaxis == "Y":
        UsdGeom.SetStageUpAxis(stage, UsdGeom.Tokens.y)
    if upaxis == "Z":
        UsdGeom.SetStageUpAxis(stage, UsdGeom.Tokens.z)

def set_reference(stage, reference, destination_path, prim_path=None, rel_paths=None):
    """
    Allows you to reference in a file and graft it into the scene graph.
    You could loop this for multiple files

    Creates a reference with the xform prims required, this does not create
    an over.

    Args:
        stage:            an opened stage file via Usd.Stage.Open()
        reference:        the usd file you want to set as the reference
        destination_path: where in the stage hierarchy to place the reference
        prim_path:        specify the position of the prim you want to reference, otherwise the default prim will be used
                          call set_default_prim(usdfile) to set the default before running set_reference()
        rel_paths:        When true, the path will be set to relative

    Returns:
        None
    """
    # get the path to the stage file and create a rel path
    if rel_paths:
        # python rel paths are ..\ but usd want .\ so we strip [1:]
        stage_file_path = stage.GetRootLayer().realPath
        reference = os.path.relpath(reference, stage_file_path)[1:]

    # create destination prim path if needed
    refFile = stage.GetPrimAtPath(destination_path)
    if not refFile:
        print(f"No path: {destination_path} creating path...")
        # create the prim path required
        refFile = stage.DefinePrim(destination_path, "Xform")

    # Set the Reference
    if prim_path == None:
        refFile.GetReferences().AddReference(reference)
    else:
        refFile.GetReferences().AddReference(
            assetPath=reference, primPath=prim_path
        )  # OPTIONAL: Reference a specific target prim. Otherwise, uses the referenced layer's defaultPrim.

def add_payload(stage, payload_path, dest_prim_path, prim_path=None, rel_paths=False):
    """
    Place a payload into a stage. A payload is a reference with a weaker opinion.

    Args:
        stage (Usd.Stage):    An opened stage file via Usd.Stage.Open()
        payload_path (str):   the path to the payload usd file
        dest_prim_path (str): Prim path for payload
        prim_path (str):      specify the position of the prim inside the payload usd file,
                              otherwise the default prim will be used.
                              Call add_default_prim(usdfile) to set the default before running add_payload()
    """
    # create rel path
    if rel_paths:
        # python rel paths are ..\ but usd want .\ so we strip [1:]
        stage_file_path = stage.GetRootLayer().realPath
        payload_path = os.path.relpath(payload_path, stage_file_path)[1:]

    # Place in existing prim_path
    payloadPrim = stage.GetPrimAtPath(dest_prim_path)
    if not payloadPrim:
        print(f'No path: {dest_prim_path} creating path...')
        # create the prim path required
        payloadPrim = stage.DefinePrim(dest_prim_path, 'Xform')

    # TODO: Specify the payload role. default, geometry, inherit, prototype, material, subset
    # payloadPrim.SetPayload(source_usd_file, "geometry")

    payloads = payloadPrim.GetPayloads()
    if prim_path == None:
        payloads.AddPayload(assetPath=payload_path)
    else:
        payloads.AddPayload(
        assetPath=payload_path,
        primPath=prim_path # OPTIONAL: Uses a specific target prim. Otherwise, uses the payload layer's defaultPrim.
    )

def save_as(stage, output_file):
    """ 
    USD doesnt have native .SaveAs() method so this just takes a modified stage
    and transfers the data to a new file allowing you to save it elsewhere.

    Args:
        stage:             An opened stage file via Usd.Stage.Open()
        output_file (str): Path to new save file.
    
    Returns:
        None
    """ 
    # Create a new stage to save the modified contents
    new_stage = Usd.Stage.CreateNew(output_file)

    # Define the root layer of the new stage
    new_root_layer = new_stage.GetRootLayer()
    
    # Transfer the contents from the original stage to the new stage
    new_root_layer.TransferContent(stage.GetRootLayer())
    
    # Save the new stage to the specified output file
    new_stage.GetRootLayer().Save()

def set_asset_info(stage, asset_info, prim=None):
    """
    Take an asset_info dictionary and adds it to the usd root_layer

    asset_info = {"identifier": "@../path/to/usdfile.usd@",
                  "name": "AssetName",
                  "version": "003"}

    Args:
        stage:             an opened stage file via Usd.Stage.Open()
        asset_info (dict): example above

    Returns:
        None
    """
    if prim is None: 
        root_path = get_hierarchy(stage)[0].GetPath()
        prim = stage.GetPrimAtPath(root_path)
    # convert prim_path string to prim
    elif isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)

    prim.SetAssetInfo(asset_info)
# endregion

# region PRIMS
def get_default_prim(stage):
    """
    Get the default prim of the usd file

    Args:
        stage: an opened stage file via Usd.Stage.Open()

    Returns:
        Default Prim
    """
    # get DefaultPrim path
    default_prim = stage.GetDefaultPrim().GetPath()
    if default_prim:
        print("Stage has DefaultPrim:", default_prim)
        return default_prim
    else:
        print('No default prim, consider running "set_default_prim()"')
        return None

def set_default_prim(stage, prim_path=None):
    """
    Adds the default prim assignment to the specified file.

    Args:
        stage:      an opened stage file via Usd.Stage.Open()
        prim_path:  the internal hierarchy path if you know it, if None it sets the top level prim path

    Returns:
        Default Prim
    """
    default_prim = stage.GetDefaultPrim().GetPath()
    if default_prim:
        print("Stage has DefaultPrim:", default_prim)
        return default_prim
    else:
        # if we dont author the default prim we must specify the root prim path
        if prim_path == None:
            # we get the first object in the hierarchy and get its path
            root_path = get_hierarchy(stage)[0].GetPath()
            root = stage.GetPrimAtPath(root_path)
        if prim_path != None:
            root = stage.GetPrimAtPath(prim_path)
        stage.SetDefaultPrim(root)
        print("DefaultPrim set to:", root)
        if isinstance(root, Usd.Prim):
            return root.GetName()
        else:
            return root

def set_prim_kind(stage, prim_path, prim_kind, recursive=False):
    """
    Set the prim at the specified path to the specified kind.

    - "group": Indicates that the prim is a group or collection of other prims.
    - "component": Indicates that the prim is a component or part of a larger assembly.
    - "assembly": Indicates that the prim is an assembly or a collection of components.
    - "subcomponent": Indicates that the prim is a subcomponent or subset of a larger component.

    Args:
        stage:            an opened stage file via Usd.Stage.Open()
        prim_path (str):  the prim path to the primitive such as '/Set/Set_Name'
        kind (str):       the kind as definded above
        recursive (bool): when true sets the kind for all prims in path.

    Returns:
        None
    """
    prim_kind = prim_kind.lower()
    if prim_kind not in ["group", "component", "assembly", "subcomponent"]:
        raise Exception("The specified Kind is not valid.")

    if recursive:
        prim_names = prim_path.split("/")
        current_path = ""

        for prim in prim_names:
            if prim:
                current_path = current_path + f"/{prim}"

                prim = stage.GetPrimAtPath(current_path)
                if not prim.IsValid():
                    raise Exception("The specified prim is not valid.")

                prim.SetMetadata("kind", prim_kind)
    else:
        prim = stage.GetPrimAtPath(prim_path)
        if not prim.IsValid():
            raise Exception("The specified prim is not valid.")

        prim.SetMetadata("kind", prim_kind)

def set_prim_type(stage, prim_path, prim_type, recursive=False):
    """
    Set the prim at the specified path to the specified type.

    Some common types are: "Xform", "Mesh", "Scope", "Camera", etc

    TODO: check if a type exists and not over write it

    Args:
        stage:            an opened stage file via Usd.Stage.Open()
        prim_path (str):  the prim path to the primitive such as '/Set/Set_Name'
        prim_type (str):  the type as definded above
        recursive (bool): when true sets the kind for all prims in path.

    Returns:
        None
    """
    prim_type = prim_type.capitalize()

    # Set default type
    # if not prim_type:
    #     prim_type = "Xform"

    if recursive:
        prim_names = prim_path.split("/")
        current_path = ""

        for prim in prim_names:
            if prim:
                current_path = current_path + f"/{prim}"

                prim = stage.GetPrimAtPath(current_path)
                if not prim.IsValid():
                    raise Exception("The specified prim is not valid.")

                prim.SetTypeName(prim_type)
    else:
        prim = stage.GetPrimAtPath(prim_path)
        if not prim.IsValid():
            raise Exception("The specified prim is not valid.")

        prim.SetTypeName(prim_type)

def set_prim_name(stage, prim, new_name):
    """ 
    Renames a primitive. This only works on the give prim or the
    last prim in the prim path.

    NOTE: This function has been replaced by rename_prim() but lives here
          as an example of how to use Sdf.BatchNamespaceEdit()

    Args:
        prim (str or obj): the address to the primitive such as '/set/setname'
                           or the prim object itself
        name (str):        only the new name for the primitive. Not the whole path.

    Returns:
        None
    """
    # check if prim is a str path or object
    if isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)
    else:
        prim_path = str(prim.GetPath())
    
    path_parts = prim_path.split("/")[:-1]
    new_primpath = f"{'/'.join(path_parts)}/{new_name}"

    if prim:
        layer = stage.GetEditTarget().GetLayer()
        edit = Sdf.BatchNamespaceEdit()
        edit.Add(prim_path, new_primpath)
        edit.Add(prim_path, Sdf.Path.emptyPath)  # Prim remove

        before = layer.ExportToString()
        print('Can we apply this rename?', layer.CanApply(edit))
        assert layer.Apply(edit), 'Rename Failed'
        after = layer.ExportToString()
        if before != after:
            print('Rename Succesful')

        # Apply the namespace edit to the stage
        stage.GetRootLayer().Apply(edit)

def rename_prim(stage, prim, new_name):
    """ 
    Renames a primitive. This only works on the give prim or the
    last prim in the prim path.

    Args:
        prim (str or obj): the address to the primitive such as '/set/setname'
                           or the prim object itself
        name (str):        only the new name for the primitive. Not the whole path.

    Returns:
        None
    """
    # check if prim is a str path or object
    if isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)
    else:
        prim_path = str(prim.GetPath())
    
    layer = stage.GetRootLayer()
    prim_path = prim.GetPath()
    prim_spec = layer.GetPrimAtPath(prim_path)
    prim_spec.name = new_name

def get_prim_type(stage, prim):
    """
    Gets the type, if any, of the given primitive

    Some common types are: "Xform", "Mesh", "Scope", "Camera", etc

    Args:
        stage:                   an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim):  the address to the primitive such as '/Set/Set_Name'

    Returns:
        prim_type or None
    """
    if isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)
    
    prim_type = prim.GetTypeName()

    if prim_type == "":
        print('Has no Type')
        return None
    else:
        return prim_type

def get_prim_kind(stage, prim):
    """
    gets the kind, if any, of the given primitive
    
    this is identical to just calling prim.GetMetadata('kind')
    its just wrapped in a function

    Args:
        stage:                   an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim):  the address to the primitive such as '/Set/Set_Name'

    Returns:
        prim_kind or None
    """
    if isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)

    prim_kind = prim.GetMetadata('kind')

    if prim_kind is None:
        print('Has no Kind')
        return None
    else:
        return prim_kind

def set_purpose(stage, prim, purpose):
    """ 
    Set the purpose of a prim. Commonly this is 'proxy', 'render' & 'guide', 'default_'.

    Args:
        stage:                       an opened stage file via Usd.Stage.Open()
        prim_name (str or Usd.Prim): the address to the primitive such as '/cube_model'
                                     or the prim object itself
        new_name (str):              new name for the primitive

    Returns:
        bool: True if the prim was successfully removed, False otherwise
    """
    # convert string to prim
    if isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)

    if prim and prim.IsA(UsdGeom.Imageable):
        purpose_attr = prim.GetAttribute(UsdGeom.Tokens.purpose)
        if purpose_attr:
            if purpose == 'proxy':
                purpose_attr.Set(UsdGeom.Tokens.proxy)
            if purpose == 'render':
                purpose_attr.Set(UsdGeom.Tokens.render)
            if purpose == 'guide':
                purpose_attr.Set(UsdGeom.Tokens.guide)
            if purpose == 'default_':
                purpose_attr.Set(UsdGeom.Tokens.default_)
            elif purpose not in ['proxy', 'render', 'guide', 'default_']:
                purpose_attr.Set(UsdGeom.Tokens.default_)
            return True
    else:
        return False

def set_relationship(stage, prim, target_prim, target_relationship):
    """ 
    Give a prim a relationship to another prim target.
    
    Commonly this is 'simproxy', 'proxyPrim', 'material:binding', 'geometry:subset'.

    - [Pixar Relationship Docs](https://openusd.org/release/glossary.html#usdglossary-relationship)
    - [OpenUSD.org API Docs](https://openusd.org/dev/api/class_usd_relationship.html)

    Args:
        stage:                         an opened stage file via Usd.Stage.Open()
        prim_name (str or Usd.Prim):   the prim path to the primitive such as '/cube_model'
                                       or the prim object itself
        target_prim (str or Usd.Prim): path to relationship target
        target_relationship (str):     'simproxy', 'proxyPrim', etc

    Returns:
        bool: True if the prim was successfully removed, False otherwise
    """
    # allow for some human error
    target_relationship = target_relationship.lower()
    if target_relationship in ['material', 'shader', 'mat']:
        target_relationship = 'material:binding'

    if target_relationship not in ['simproxy', 'proxyPrim', 'material:binding', 'geometry:subset']:
        raise Exception("The specified relationship is not valid.")

    # convert prim_path string to prim
    if isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)

    # convert target_prim to path string
    if not isinstance(target_prim, str):
        target_prim = str(target_prim.GetPath())

    if prim:
        if target_relationship == 'material:binding':
            UsdShade.MaterialBindingAPI.Apply(prim)

        prim_rel = prim.CreateRelationship(target_relationship, False)

        # Add the target to the relationship creates just 'rel xxx = <'
        prim_rel.SetTargets([Sdf.Path(target_prim)])

        # Other Options we can use
        # prim_rel.AddTarget(target_prim) # creates a 'prepend rel xxx = <'
        # prim_rel.ClearTargets() - Remove all opinions about the target list from the current edit target.
        # prim_rel.GetTargets()
        # prim_rel.RemoveTarget()- Remove all opinions about the target list from the current edit target.
        return True
    else:
        return False

def set_extent(stage, prim):
    """
    Calculates & sets the extentsHint of a prim based on its World Bounds

    Args:
        stage:                     an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim):    the prim path to the primitive such as '/root/cube_model'
                                   or the prim object itself

    Returns:
        extents_hint (Vec3fArray): two-by-three array containing the endpoints of the
                                   axis-aligned, object-space extent 
    """ 
    # convert prim_path string to prim
    if isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)

    bbox_cache = UsdGeom.BBoxCache(Usd.TimeCode.Default(),
                            includedPurposes=[UsdGeom.Tokens.default_,
                                                UsdGeom.Tokens.render],
                            useExtentsHint=False,
                            ignoreVisibility=False)

    # Compute the world bounds of the primitive
    bbox_bounds = bbox_cache.ComputeWorldBound(prim)
    bound_align = bbox_bounds.ComputeAlignedBox()
    bbox_min = bound_align.GetMin()
    bbox_max = bound_align.GetMax()

    def convert_to_float(number):
        # Corrects for scientific notation floating-point formatting issues
        if "e" in str(number):
            string_min = str(number)
            correction = string_min.split("e")
            return float(correction[0]) * (10 ** int(correction[1]))
        return number

    # Correct the bounding box values
    corrected_min = [convert_to_float(number) for number in bbox_min]
    corrected_max = [convert_to_float(number) for number in bbox_max]

    # Compute extents
    extents = [corrected_min,corrected_max]

    # set an extents hint
    extents_hint = prim.CreateAttribute("extent", Sdf.ValueTypeNames.Vector3fArray)
    extents_hint.Set(extents)
    return extents_hint

def get_extent(stage, prim):
    """
    Gets the extentsHint of a prim

    [Pixar Inspect Tutorial](https://openusd.org/release/tut_inspect_and_author_props.html)

    Args:
        stage:                      an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim):     the address to the primitive such as '/root/cube_model'
                                    or the prim object itself

    Returns:
        extents_hint (Vec3fArray): two-by-three array containing the endpoints of the
                                   axis-aligned, object-space extent
    """
    # convert prim_path string to prim
    if isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)

    extents_hint = prim.GetAttribute('extent')
    return extents_hint.Get()

def set_prim_visibility(stage, prim, visibility=True):
    """
    Set the prim visibility attribute

    Args:
        stage:                  an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim): the prim path to the primitive such as '/root/cube_model'
                                or the prim object itself
        visibility (bool):      True = Visible, False = Invisible                                    

    Returns:
        None  
    """
    # convert prim_path string to prim
    if isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)

    # set vis toggle
    if visibility:
        visibility = "visible"
    else:
        visibility = "invisible"

    if prim:
        prim.CreateAttribute("visibility", Sdf.ValueTypeNames.Token).Set(visibility)

def add_inherit(stage, prim, inherit_class):
    """
    Allows you to add an inhert class to an existing usd prim.

    An inherit enables a prim to contain information that the base prim inherits.
    [Pixar Inherit Docs](https://openusd.org/release/glossary.html#usdglossary-inherits)

    Args:
        stage:                   an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim):  the prim path to the primitive such as '/cube_model'
                                 or the prim object itself
        inherit_class (Usd.Prim): the prim of the class you wish to inherit from
    """
    # convert prim_path string to prim
    if isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)
    
    inherits = prim.GetInherits()
    inherits.AddInherit(inherit_class.GetPath())

def extract_prim_to_file(stage, prim_path, output_file, output_prim_path=None):
    """ 
    Extracts the given prim at the prim_path out to its own usd file
    If output_prim_path is specified it will repath the prim from the source
    to a new prim_path, otherwise it maintains the original path.

    Thanks to [USD Survival Guide](https://lucascheller.github.io/VFX-UsdSurvivalGuide/production/concepts.html#sdfCopySpec)

    Args:
        stage:                  an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim): the address to the primitive such as '/cube_model'
                                or the prim object itself
        output_file (str):      the destination location for the .usd
        output_prim_path (str): the path to a new prim output path '/asset/cube_model'

    Returns:
        None
    """
    # convert prim_path to str if not
    if not isinstance(prim_path, str):
        prim_path = str(prim_path.GetPath())
    prim_path = prim_path.rstrip("/")

    # Get the source layer and path
    source_layer = stage.GetRootLayer()
    source_path = Sdf.Path(prim_path)

    # Create a new layer and placeholder prim for the destination
    new_stage = Usd.Stage.CreateInMemory()
    destination_layer = new_stage.GetEditTarget().GetLayer()
    new_prim = new_stage.DefinePrim(source_path, "Xform")

    # Copy the prim
    if output_prim_path:
        new_prim = new_stage.DefinePrim(output_prim_path, "Xform")
        Sdf.CopySpec(source_layer, source_path, destination_layer, output_prim_path)
        # removes old prim paths from new file
        remove_prim(new_stage, source_path, recursive=True)
    else:
        Sdf.CopySpec(source_layer, source_path, destination_layer, source_path)
    
    destination_layer.Export(output_file)

# endregion

# region MESH
def get_point_positions(stage, prim, print_values=False):
    """
    Get the point positions of an object in a stage

    Args:
        stage:                  an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim): the address to the primitive such as '/root/cube_model'
                                or the prim object itself

    Returns:
        point_positions (point3f[]): the point positions
    """ 
    # convert prim_path string to prim
    if isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)

    # Can only get points on Mesh objects
    prim_type = get_prim_type(stage, prim)

    if prim_type == 'Mesh':
        mesh = UsdGeom.Mesh(prim)
        
        # Access the vertex positions
        points_attribute = mesh.GetPointsAttr()
        point_positions = points_attribute.Get()

        # Print vertex positions
        if print_values:
            for index, position in enumerate(point_positions):
                print(f'Point {index}: {position}')

        return point_positions
    else:
        print(f"Prim \"{prim.GetName()}\" is not a Mesh object")

def set_point_positions(stage, prim, point_positions):
    """
    Sets the point positions of the prim object.

    Documentation: Two methods are available, we have implemented the first method
    ### Method 01: for simply giving the new positions
    ```
    new_vertex_positions = [(-5, 0, -5), (0, 0, -5), (5, 0, -5), (-5, 0, 0)]
    points_attribute.Set(new_vertex_positions)
    ```
    
    ### Method 02: Shows we can modify the positions based on index
    ```
    for i, vertex_position in enumerate(current_vertex_positions):
        # Example: Multiply each coordinate by 2
        new_position = vertex_position * 2.0
        current_vertex_positions[i] = new_position
    points_attribute.Set(current_vertex_positions)
    ```

    Args:
        stage:                       an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim):      the address to the primitive such as '/root/cube_model'
                                     or the prim object itself
        point_positions (point3f[]): a single list containing multiple Gf.Vec3d tuples per point
                                     eg. [(-5, 0, -5), (0, 0, -5), (5, 0, -5), (-5, 0, 0)]

    Returns:
        None    
    """ 
    # convert prim_path string to prim
    if isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)

    # Can only get points on Mesh objects
    prim_type = get_prim_type(stage, prim)

    if prim_type == 'Mesh':
        mesh = UsdGeom.Mesh(prim)

        # Access the Points attribute
        points_attribute = mesh.GetPointsAttr()
        
        # Get the current vertex positions
        current_vertex_positions = points_attribute.Get()

        if len(current_vertex_positions) == len(point_positions):
            print ("Point Counts Match - Applying new point positions")

            # Set the new positions
            points_attribute.Set(point_positions)
        else:
            print("Point Count do not Match")
    else:
        print(f"Prim \"{prim.GetName()}\" is not a Mesh object")

def create_geomsubset(stage, prim_path, subset_name, points):
    """
    Creates a geomsubset that includes the given vertices or points
    from a list, eg: [0, 1, 2, 3, 4, 5]

    Note: A geomsubset does not allow you to author visibility for it or override its purpose

    Args:
        stage:                  an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim): the address to the primitive such as '/cube_model'
                                or the prim object itself
        subset_name (str):      the name for your subset
        points (list):          a list containing your point numbers

    Returns:
        None
    """
    # convert prim to prim_path (str)
    if not isinstance(prim, str):
        prim_path = str(prim.GetPath())
    else:
        prim_path = prim.rstrip("/")

    mesh = UsdGeom.Mesh.Define(stage, prim_path)

    # Creating a geom subset for faces
    element_type = UsdGeom.Tokens.face
    indices = Vt.IntArray(points)
    # family_name = "YourFamilyName" # Optional
    # family_type = "YourFamilyType" # Optional

    subset = UsdGeom.Subset.CreateGeomSubset(
        mesh, subset_name, element_type, indices #, family_name, family_type
    )

# endregion

# region VARIANTS
def set_variant(stage, prim, variant_set, variant_name):
    '''   
    This function sets the variant on the specified prim.
    Variants live within a Variant Set, so both are required.

    Args:
        stage:                  an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim): the address to the primitive such as '/cube_model'
        variant_set (str):      the variant set name
        variant_name (str):     the variant name

    Returns:
        None
    '''
    # convert prim_path string to prim
    if isinstance(prim, str):
        prim_path = prim.rstrip("/")
        prim = stage.GetPrimAtPath(prim_path)

    # Get the variant set & set variant
    variant_set = prim.GetVariantSet(variant_set)
    variant_set.SetVariantSelection(variant_name)

def create_variant_file(stage, prim_path, variant_set, variant_files, rel_paths=None):
    """
    Creates a new usd file containing the given usd files as references under a single
    new prim where each file is a new variant. The variant name is pulled from the
    usd file itself.

    Args:
        stage:                an opened stage file via Usd.Stage.Open()
        prim_path (str):      the name of the top level asset name
        variant_set (str):    the variant set name
        variant_files (list): A list of the paths including filenames of the variant files
        rel_paths: When true, the path will be set to relative
    """
    # check prim_path is string
    if isinstance(prim_path, str):
        prim_path = prim_path.rstrip("/")

    # Create prim at prim_path
    UsdGeom.Xform.Define(stage, prim_path)
    prim = stage.GetPrimAtPath(prim_path)

    # Create a variant set on the prim
    variant_set = prim.GetVariantSets().AddVariantSet(variant_set)

    # set the root prim as default prim if stage has none
    if get_default_prim(stage) is None:
        stage.SetDefaultPrim(prim)

    for variantFile in variant_files:
        var_file = Usd.Stage.Open(variantFile)

        # check if variant files have default prims
        if get_default_prim(var_file) is None:
            print('If you get a "Unresolved reference prim path" error\nIts becuase USD is not finding a default prim\nThis is required!\n')

        hierarchy = get_hierarchy(var_file)
        var_name = hierarchy[0].GetName()

        if rel_paths:
            # python rel paths are ..\ but usd want .\ so we strip [1:]
            stage_file_path = stage.GetRootLayer().realPath
            variantFile = os.path.relpath(variantFile, stage_file_path)[1:]

        # we need to specify the var name
        variant_set.AddVariant(var_name)
        variant_set.SetVariantSelection(var_name)

        # Add items or changes per each Variant
        with variant_set.GetVariantEditContext():
            prim.GetPrim().GetReferences().AddReference(variantFile)
            # # You can set positions on these prims if you want
            # UsdGeom.XformCommonAPI(prim).SetTranslate((4, 5, 6))

def get_variants(stage, prim):
    '''
    This function returns a dict of the variant sets & variants of the
    specified prim.

    Args:
        stage:                          an opened stage file via Usd.Stage.Open()
        prim (str, Sdf.Path, Usd.Prim): the address to the primitive such as '/cube_model'

    Returns:
        variant_output (dict[list]):    a dictionary with a list of each variant
    '''
    # check prim [0] is prim, [1] is prim_path
    prim = get_prim_and_path(stage, prim)[0]
    
    variant_output = {}
    variant_sets = prim.GetVariantSets()
    for variant_set in variant_sets.GetNames():
        # print('VarSet:', variant_set)
        variant_output[variant_set] = []
        variants = prim.GetVariantSet(variant_set).GetVariantNames()
        for variant in variants:
            # print('Var:', variant)
            variant_output[variant_set].append(variant)

    return variant_output

# endregion

# region HIERARCHY
def get_hierarchy(stage):
    """
    Get the hierarchy of the usd file

    Args:
        stage: an opened stage file via Usd.Stage.Open()

    Returns:
        The stage hierarchy
    """
    hierarchy = []
    for prim_path in stage.Traverse():
        hierarchy.append(prim_path)
    return hierarchy

def remove_prim(stage, prim_path, recursive=None):
    """
    Remove the specified prim if it exists

    Args:
        stage:            an opened stage file via Usd.Stage.Open()
        prim_path (str):  the address to the primitive such as '/set/setname'
        recursive (bool): If True will recursively remove all prims in the path

    Returns:
        bool: True if the prim was successfully removed, False otherwise
    """
    # convert prim to prim_path if not
    if not isinstance(prim_path, str):
        # handle sdf path too
        if isinstance(prim_path, Sdf.Path):
            prim_path = prim_path.pathString
        else:
            prim_path = str(prim_path.GetPath())
    prim_path = prim_path.rstrip("/")

    prim = stage.GetPrimAtPath(prim_path)
    if prim:
        if recursive:
            components = prim_path.split('/')
            # Remove empty strings from the list if the path starts with '/'
            if components[0] == '':
                components = components[1:]

            # Iterate through the components, reconstructing the path
            for i in range(len(components), 0, -1):
                current_path = '/' + '/'.join(components[:i])
                stage.RemovePrim(current_path)
        else:
            success = stage.RemovePrim(prim.GetPath())
            return success
    else:
        return False

def find_prim_by_name(stage, prim_name):
    """
    Returns the prim by searching the stage for matching prim_name

    Args:
        stage:           an opened stage file via Usd.Stage.Open()
        prim_name (str): the address to the primitive such as 'cube_model'

    Returns:
        prim (Usd.Prim): the prim at its path in the stage.
    """
    for prim in stage.Traverse():
        if prim.GetName() == prim_name:
            return prim
    return None

def modify_hierarchy(stage, current_primpath, new_primpath, remove=True):
    """
    WIP Function to modify the hierarchy of a stage.

    Args:
        stage:                  an opened stage file via Usd.Stage.Open()
        current_primpath (str): the prim path you wish to modify
        new_primpath (str):     the resulting prim path
        remove (bool):          True will remove the current_primpath from the stage

    Returns:
        None
    """ 
    # clean / off paths
    current_primpath = current_primpath.rstrip("/")
    new_primpath = new_primpath.rstrip("/")

    # prep layer for namespace edit
    layer = stage.GetEditTarget().GetLayer()
    edit = Sdf.BatchNamespaceEdit()
    
    # create new path so we can parent to it
    UsdGeom.Xform.Define(stage, new_primpath)

    # TODO: check if a type exists and not over write it
    set_prim_type(stage, new_primpath, 'Xform', recursive=True)
    
    # get any children prims at this path
    prim = stage.GetPrimAtPath(current_primpath)
    if prim.IsValid():
        children = prim.GetChildren()
        for child in children:
            child_name = child.GetName()
            child_path = child.GetPath()
            output_primpath = f"{new_primpath}/{child_name}"

            # add edit to the layer
            edit.Add(child_path, output_primpath)
    
    # Remove old path
    if remove:
        edit.Add(current_primpath, Sdf.Path.emptyPath)  # Prim remove

    before = layer.ExportToString()
    print('Can we apply this modify', layer.CanApply(edit))
    assert layer.Apply(edit), 'Failed to Modify'
    after = layer.ExportToString()
    if before != after:
            print('Rename Succesful')

    # Apply the namespace edit to the stage
    stage.GetRootLayer().Apply(edit)

# endregion

# region CHECKS
def prim_is_xform(prim):
    """
    Check to see if a prim is an xform.

    Args:
        prim (str): a prim path for example inside a loop 'for prim in stage.Traverse():'

    Returns:
        True if prim is xform
    """
    if prim.IsA(UsdGeom.Xform):
        return True
    return False

def prim_is_geometry(prim):
    """
    Check to see if a prim contains Geometry.
    Mesh prims in USD are typically wrapped in a UsdGeom.Mesh schema, which inherits from UsdGeom.Geometry.

    Args:
        prim (str): a prim path for example inside a loop 'for prim in stage.Traverse():'

    Returns:
        True if prim contains Geometry
    """
    # return prim.IsA(UsdGeom.Xform) or prim.IsA(UsdGeom.Mesh)
    if prim.IsA(UsdGeom.Mesh):
        return True
    mesh_schema = UsdGeom.Mesh(prim)
    if mesh_schema:
        return True
    return False

def prim_is_material(prim):
    """
    Check to see if a prim contains Material.=

    Args:
        prim (str): a prim path for example inside a loop 'for prim in stage.Traverse():'

    Returns:
        True if prim contains Material
    """
    if prim.IsA(UsdShade.Material):
        return True
    return False

def prim_isA(prim, prim_type):
    """ 
    A wrapper for the `prim.IsA()`

    Supported prim types are: "Xform", "Mesh", "Geo", "Material"
    TODO: "Scope", "Camera"

    Args:
        prim (str):      a prim path for example inside a loop 'for prim in stage.Traverse():'
        prim_type (str): string containing type you want to test for
    
    Returns:
        True if prim matches prim_type
    """
    prim_type = prim_type.capitalize()

    if prim_type == "Xform":
        if prim.IsA(UsdGeom.Xform):
            return True
    elif prim_type in ["Mesh", "Geo"]:
        if prim.IsA(UsdGeom.Mesh):
            return True
        mesh_schema = UsdGeom.Mesh(prim)
        if mesh_schema:
            return True
    elif prim_type == "Material":
        if prim.IsA(UsdShade.Material):
            return True
    else:
        return False


# endregion

# region SRTs
def set_sca_rot_tran(stage, prim_path, scale=None, rotation=None, translation=None):
    """
    Set the Scale, Rotation and Translation (SRT) of the specified prim.

    Args:
        stage:             an opened stage file via Usd.Stage.Open()
        prim_path (str):   the address to the primitive such as '/Set/Set_Name'
        scale (list):       the [x, y, z] scale for the primitive
        rotation (list):    the [x, y, z] rotation for the primitive
        translation (list): the [x, y, z] coordinates for the primitive

    Returns:
        None
    """
    prim = stage.GetPrimAtPath(prim_path)

    if scale:
        UsdGeom.XformCommonAPI(prim).SetScale(scale)
    if rotation:
        UsdGeom.XformCommonAPI(prim).SetRotate(rotation)
    if translation:
        UsdGeom.XformCommonAPI(prim).SetTranslate(Gf.Vec3d(translation))

# endregion

# region LIGHTS
def create_light(stage, prim_path, light_type, color, intensity, exposure, specular,
    radius=None,
    cone_angle=None,
    cone_softness=None,
    texture_file=None,
    texture_width=None,
    rel_paths=None,
):
    """
    Create a light in stage at specified prim path

    Lights
    - "DiskLight":    Creates a Spot Light in Houdini, supports radius, cone_angle & cone_softness values.
    - "SphereLight":  Creates a Point Light in Houdini although Blender enables cone_angle making this act like a spot.
    - "DistantLight": Creates a Distant Light in Houdini or Sun Light in Blender
    - "RectLight":    Creates a Area Light, supports a texture file
    - "DomeLight":    Creates a Dome Light in Houdini which is similar to Environment Light, supports a texture file
                      Blender 4.0.0 Alpha exports its Environment Light to '/lights/environment' as a DomeLight

    Args:
        stage:                 an opened stage file via Usd.Stage.Open()
        prim_path (str):       the address to the primitive such as '/lights/LightName'
        color (3fArray):       the color as a list [R, G, B]
        intensity (float):     the intensity
        exposure (float):      the exposure
        specular (float):      the specular
        radius (float):        DiskLight Only: the radius of the light
        cone_angle (float):    DiskLight Only: the frustum angle of the light
        cone_softness (float): DiskLight Only: the edge of the cone softeness value
        texture_file (str):    RectLight & DomeLight: the path and file name to texture file
        texture_width (float): RectLight Only: the texture width

    Returns:
        None
    """
    if light_type not in [
        "DiskLight",
        "SphereLight",
        "RectLight",
        "DistantLight",
        "DomeLight",
    ]:
        raise ValueError("The specified light_type is not valid.")

    # get the path to the stage file and create a rel path
    if rel_paths:
        # python rel paths are ..\ but usd want .\ so we strip [1:]
        stage_file_path = stage.GetRootLayer().realPath
        texture_file = os.path.relpath(texture_file, stage_file_path)[1:]

    light_prim = stage.DefinePrim(prim_path, light_type)

    if color:
        light_prim.CreateAttribute("inputs:color", Sdf.ValueTypeNames.Color3fArray).Set(color) # Gf.Vec3d(color)
    if intensity:
        light_prim.CreateAttribute("inputs:intensity", Sdf.ValueTypeNames.Float).Set(intensity)
    if exposure:
        light_prim.CreateAttribute("inputs:exposure", Sdf.ValueTypeNames.Float).Set(exposure)
    if specular:
        light_prim.CreateAttribute("inputs:specular", Sdf.ValueTypeNames.Float).Set(specular)

    if light_type == "DiskLight":
        if radius:
            light_prim.CreateAttribute("inputs:radius", Sdf.ValueTypeNames.Float).Set(radius)
        if cone_angle:
            light_prim.CreateAttribute("inputs:shaping:cone:angle", Sdf.ValueTypeNames.Float).Set(cone_angle)
        if cone_softness:
            light_prim.CreateAttribute("inputs:shaping:cone:softness", Sdf.ValueTypeNames.Float).Set(cone_softness)

    if light_type == "RectLight":
        if texture_file:
            light_prim.CreateAttribute("inputs:texture:file", Sdf.ValueTypeNames.Asset).Set(texture_file)
        if texture_width:
            light_prim.CreateAttribute("inputs:width", Sdf.ValueTypeNames.Float).Set(texture_width)

    if light_type == "DomeLight":
        if texture_file:
            light_prim.CreateAttribute("inputs:texture:file", Sdf.ValueTypeNames.Asset).Set(texture_file)
            light_prim.CreateAttribute("inputs:texture:format", Sdf.ValueTypeNames.Token).Set("automatic")

    return light_prim

# endregion

# region LOOKDEV
def create_usdpreviewsurface(stage, material_path, diffuseColor, emissiveColor=None, roughness=None,
    specular=None,
    opacity=None,
    opacityThreshold=None,
    metallic=None,
    ior=None,
    clearcoat=None,
    clearcoatRoughness=None):
    """
    Creates a simple usd preview material with the specified values.
    Currently it does not support image texture files.

    [UsdPreviewSurface Specification](https://openusd.org/release/spec_usdpreviewsurface.html)

    Args:
        stage:                      an opened stage file via Usd.Stage.Open()
        material_path (str):        the scene path to store the material, commonly /mtl
        diffuseColor (3fArray):     the color as a tuple (R, G, B)
        emissiveColor (3fArray):    the color as a tuple (R, G, B)
        roughness (float):          the roughness
        specular (float):           the specular
        opacity (float):            the opacity, 1.0 is fully opaque
        opacityThreshold (float):   the opacityThreshold input is useful for creating geometric cut-outs based on the opacity input
        metallic (float):           the metallic value
        ior (str):                  the Index of Refraction, used on translucent objects
        clearcoat (float):          the second specular
        clearcoatRoughness (float): the roughness for the second specular

    Returns:
        None
    """
    # material_prim = stage.DefinePrim(material_path, 'Material')
    material_prim = UsdShade.Material.Define(stage, material_path)

    preview_surface = UsdShade.Shader.Define(stage, material_path + '/PreviewSurface')
    preview_surface.CreateIdAttr('UsdPreviewSurface')

    # Set the values
    if diffuseColor:
        preview_surface.CreateInput('diffuseColor', Sdf.ValueTypeNames.Color3f).Set(diffuseColor) #Gf.Vec3f(0.5, 0, 0)
    if emissiveColor:
        preview_surface.CreateInput('emissiveColor', Sdf.ValueTypeNames.Color3f).Set(emissiveColor) #Gf.Vec3f(0.5, 0, 0)
    if roughness:
        preview_surface.CreateInput("roughness", Sdf.ValueTypeNames.Float).Set(roughness)
    if specular:
        preview_surface.CreateInput("specular", Sdf.ValueTypeNames.Float).Set(specular)
    if opacity:
        preview_surface.CreateInput("opacity", Sdf.ValueTypeNames.Float).Set(opacity)
    if opacityThreshold:
        preview_surface.CreateInput("opacityThreshold", Sdf.ValueTypeNames.Float).Set(opacityThreshold)
    if metallic:
        preview_surface.CreateInput("metallic", Sdf.ValueTypeNames.Float).Set(metallic)
    if ior:
        preview_surface.CreateInput("ior", Sdf.ValueTypeNames.Float).Set(ior)
    if clearcoat:
        preview_surface.CreateInput("clearcoat", Sdf.ValueTypeNames.Float).Set(clearcoat)
    if clearcoatRoughness:
        preview_surface.CreateInput("clearcoatRoughness", Sdf.ValueTypeNames.Float).Set(clearcoatRoughness)

    # Connect the shader to the material's surface output
    material_prim.CreateSurfaceOutput().ConnectToSource(preview_surface.ConnectableAPI(), 'surface')

# endregion

# region ANIM
def get_frame_count(stage, prim):
    """ 
    Returns the number of frames for a prim in a usd file.

    Args:
        stage:                  an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim): the prim path to the primitive/s such as '/Set/Set_Name'

    Returns:
        frame_count (int):      the total number of timeSamples on the prim
    """
    # convert prim to prim_path (str)
    if not isinstance(prim, str):
        prim_path = str(prim.GetPath())
    else:
        prim_path = prim.rstrip("/")

    frame_count = 0
    for prim in stage.Traverse():
        for attr in prim.GetAttributes():
            time_samples = attr.GetTimeSamples()
            if len(time_samples) > 0:
                max_frame = int(max(time_samples))
                frame_count = max(frame_count, max_frame)
    return frame_count

# endregion

# region EXPERIMENTAL
def brute_rename(stage, prim_name, new_name):
    """ 
    Making sdf layer namespace edits can be rather challenging. This allows a more
    'brute force' method of renaming a prim. Its a simple text search & replace
    which does possibly slow it down and of course does not conform to USD norms.

    Here be Dragons!!

    Args:
        stage:           an opened stage file via Usd.Stage.Open()
        prim_name (str): the address to the primitive such as 'cube_model'
                         or the prim object itself
        new_name (str):  new name for the primitive
    """
    # convert stage data to string
    layer = stage.GetEditTarget().GetLayer()
    text = layer.ExportToString()

    modified_lines = []
    for line in text.splitlines():
        if "def " in line:
            match = re.search(r'"([^"]+)"', line)
            if match:
                name = match.group(1)
                if name == prim_name:
                    line = line.replace(name, new_name, 1)  # Replace only the first occurrence
        modified_lines.append(line)

    modified_text = '\n'.join(modified_lines)

    stage.GetRootLayer().ImportFromString(modified_text)

def get_payloads(stage, prim=None):
    """
    gets the file path, if any, of the given primitive
    if prim is set to None it will return a list of all payloads
    found and on what prim they are found:

    payloads = [prim_name, payload]

    Args:
        stage:                   an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim):  the address to the primitive such as '/Set/Set_Name'

    Returns:
        payload (file, None or list)
    """ 
    # get primm if path supplied
    if isinstance(prim, str):
        prim_path = prim
        prim = stage.GetPrimAtPath(prim_path)

    layer = stage.GetEditTarget().GetLayer()
    contents = layer.ExportToString()

    # Regular expression patterns
    def_pattern = r'def\s+[\w\s]+"([^"]+)"'
    payload_pattern = r'prepend\s+payload\s+=\s+@([^@]+)@'

    # Find matches for def and payload lines
    def_matches = re.findall(def_pattern, contents)
    payload_matches = re.findall(payload_pattern, contents)

    # Extract payload information from the matches
    payloads = []
    for i in range(min(len(def_matches), len(payload_matches))):
        prim_name = def_matches[i]
        payload = payload_matches[i]
        payloads.append((prim_name, payload))

    if prim:
        name = prim.GetName()
        for prim_name, payload in payloads:
            if name == prim_name:
                return payload
            else:
                return None
    else:
        return payloads
# endregion

# region WIP
def add_variant(stage, prim, variant_set, variant_name):
    '''
    Work in Progress

    This function adds variants to a known variant set

    Args:
        stage:                  an opened stage file via Usd.Stage.Open()
        prim (str or Usd.Prim): the address to the primitive such as '/cube_model'
        variant_set (str):      the variant set name
        variant_name (str):     the variant name

    Returns:
        None
    ''' 
    # convert prim_path string to prim
    if isinstance(prim, str):
        prim_path = prim.rstrip("/")
        prim = stage.GetPrimAtPath(prim_path)

    # Create variant set on the prim if it doesn't exist
    variant_sets = parent_prim.GetVariantSets()
    if not variant_sets.HasVariantSet(variant_set):
        variant_set = variant_sets.AddVariantSet(variant_set)
    else:
        variant_set = variant_sets.GetVariantSet(variant_set)
    
    # add variant
    variant_set.AddVariant(variant_name)
    variant_set.SetVariantSelection(variant_name)
    with variant_set.GetVariantEditContext():
        prim = stage.GetPrimAtPath(prim)
        new_prim = stage.OverridePrim(parent_prim_path.AppendChild(prim.GetName()))
        new_prim.GetReferences().AddReference(prim.GetPath())
     
# endregion

# region MAIN
if __name__ == "__main__":
    main()
# endregion
