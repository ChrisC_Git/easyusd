# region IMPORTS
import unittest
from unittest.mock import MagicMock
from pxr import Usd, UsdGeom, Sdf

import os, sys, tempfile

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from easyusd import *

script_path = os.path.dirname(os.path.abspath(__file__))
# endregion

# region TEST STAGES
class TestCreateStage(unittest.TestCase):
    """ 
    Test the create_stage() function
    """ 
    def setUp(self):
        # Create a temporary directory to store the test files
        self.test_dir = tempfile.mkdtemp()

    def tearDown(self):
        # Clean up the temporary directory after the test
        for root, dirs, files in os.walk(self.test_dir, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
        os.rmdir(self.test_dir)

    def test_create_stage_without_scene_path(self):
        path = os.path.join(self.test_dir, 'test_stage.usda')
        stage = create_stage(path)
        self.assertIsNotNone(stage)
        self.assertTrue(os.path.exists(path))

    def test_create_stage_with_scene_path(self):
        path = os.path.join(self.test_dir, 'test_stage_with_xform.usda')
        scene_path = '/World/Xform'
        stage = create_stage(path, scene_path)
        self.assertIsNotNone(stage)
        self.assertTrue(os.path.exists(path))
        self.assertTrue(stage.GetPrimAtPath(scene_path).IsValid())
        

class TestCreatePrim(unittest.TestCase):
    """ 
    Test the create_prim() function
    """ 
    def setUp(self):
        # Mock the USD stage
        self.stage = MagicMock(spec=Usd.Stage)
        self.stage.GetPrimAtPath.return_value = None

    def test_create_prim_with_type(self):
        # Test creating a prim with a specific type
        create_prim(self.stage, '/cube_model', 'Mesh')
        self.stage.DefinePrim.assert_called_once_with('/cube_model', 'Mesh')

    def test_create_prim_recursive(self):
        # Test creating a prim recursively
        create_prim(self.stage, '/cube_model/child', 'Xform', recursive=True)
        self.stage.DefinePrim.assert_called_with('/cube_model/child', 'Xform')

    # def test_prim_already_exists(self):
    #     # Test handling a case where the prim already exists
    #     self.stage.GetPrimAtPath.return_value = MagicMock(spec=Usd.Prim)
    #     result = create_prim(self.stage, '/cube_model')
    #     self.assertFalse(result, "Prim should not be created if it already exists")

# endregion

if __name__ == '__main__':
    unittest.main()