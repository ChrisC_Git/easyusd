# INFO
"""
    Some examples of how to use EasyUSD

    A few are wrapped in functions that have thier own docstrings, however others
    are just blocks of code that have been commented out.
    You simply need to uncomment the section following a heading:
    ```
    # # === Create a stage & Add Meta Data & Sublayers ===
    # create_stage(target_usd_path, scene_path=None)
    # stage = Usd.Stage.Open(target_usd_path)
    # example_setMetadata_addSublayer(stage)
    # stage.Save()
    ```
    If you have the USD_Core downloaded you can go to /scripts/usdview.bat to view your files
"""

# region IMPORTS
import os
import sys

sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
from easyusd import *

script_path = os.path.dirname(os.path.abspath(__file__))
# endregion

# region FILES
# USD Files
target_usd_file = os.path.join(script_path, "filename.usda")
cube_usd_file = os.path.join(script_path, "standard_prims", "cube.usda")
sphere_usd_file = os.path.join(script_path, "standard_prims", "sphere.usda")
cone_usd_file = os.path.join(script_path, "standard_prims", "cone.usda")
cylinder_usd_file = os.path.join(script_path, "standard_prims", "cylinder.usda")
plane_usd_file = os.path.join(script_path, "geo_files", "plane.usda")
anim_usd_file = os.path.join(script_path, "anim_files", "suzanne_anim.usda")
lookdev_usd_file = os.path.join(script_path, "lookdev_files", "suzanne_simple_usdpreview.usda")
target_usd_path = os.path.join(script_path, target_usd_file)
material_usd_path = os.path.join(script_path, 'material.usda')

# Image Files
uv_map_file = os.path.join(script_path, "texture_files", "CustomUVChecker_byValle_1K.png")
# endregion

def example_setMetadata_addSublayer(stage):
    """
    Simple example function that sets the usd metadata
    and sublayers in two seperate usd stage files.
    """
    # set metadata
    set_metadata(stage, fps=24, timecode=24, units="meters", upaxis="Z")

    # loop add_sublayer to add multiple files
    source_usd_files = (cube_usd_file, sphere_usd_file)

    for usd_file in source_usd_files:
        set_sublayers(stage, usd_file)

def example_get_set_defaultPrim(stage):
    """
    Simple example function that gets the usd stage's
    defualt prim. If none exists it will set it to `/cube_model`.
    """
    prim_name = get_default_prim(stage)
    if prim_name is None:
        set_default_prim(stage, prim_path="/cube_model")

def example_add_references(stage):
    """
    Simple example function that instead of sublayering
    two files into a usd stage file refereces them at a specific
    branch or location in the scene hierarchy.
    """
    reference_files = (cube_usd_file, sphere_usd_file)

    for ref_file in reference_files:
        # set default prim in ref usd... remember to save the ref file, we use this below
        ref_stage = Usd.Stage.Open(ref_file)
        default_prim = set_default_prim(ref_stage)
        ref_stage.Save()

        # place file as reference at specified location, we are using the default prim here
        set_reference(
            stage,
            usd_file,
            destination_path=f"/set/setname/{default_prim}",
            prim_path=None,
            rel_paths=True,
        )

def example_creating_lights(stage):
    """
    Simple example function that creates two lights in the usd stage
    One DomeLight and one spotlight (DiskLight), each with specific values.

    It then sets the /lights to a Scope Type as well as setting the
    position and rotation of the spotlight
    """
    # specify the light prim path and name
    env_light = "/lights/environment"
    spot_light = "/lights/spotlight"

    # create the light
    create_light(
        stage,
        env_light,
        light_type="DomeLight",
        color=[1, 1, 1],
        intensity=0.8,
        exposure=0,
        specular=1,
        texture_file=uv_map_file,
        rel_paths=True,
    )
    create_light(
        stage,
        spot_light,
        light_type="DiskLight",
        color=[0.5, 0.5, 0],
        intensity=10,
        exposure=0,
        specular=1,
        radius=0.75,
    )

    # set light prim to scope (good practice)
    set_prim_type(stage, "/lights", "Scope", recursive=False)

    # position spot light
    set_sca_rot_tran(stage, spot_light, scale=None, rotation=[-45, 0, 0], translation=[0, 2, 5])

def extract_materials(stage):
    ''' 
    This example for the usage of the 'extract_prim_to_file' function:
    It takes the combined blender file that contains both a mesh
    and a material and splits out only the material into a new usd file.
    
    This example also output the material to a new material path.
    '''
    materials = []
    for prim in stage.Traverse():
        if prim.IsA(UsdShade.Material):
            materials.append(prim)

    # For each material, extract and save to a new file
    for material in materials:
        # Get the prim associated with the material
        material_prim = material.GetPrim()
        # print(material_prim)
        
        # Create a new stage for the material
        target_usd_file = f'{material_prim.GetName()}.usda'
        material_stage = os.path.join(script_path, target_usd_file)

        new_prim_path = f'/mtl/{material_prim.GetName()}'
        extract_prim_to_file(stage, material_prim, material_stage, output_prim_path=new_prim_path)

# # === Create a stage ===
# create_stage(target_usd_path, '/test/test2/')

# # === prim_exists ===
# stage = Usd.Stage.Open(target_usd_path)
# prim = '/test/test2/'
# prim_exists(stage, prim, verbose=True)

# # === Create a stage & Add Meta Data & Sublayers ===
# create_stage(target_usd_path, prim=None)
# stage = Usd.Stage.Open(target_usd_path)
# example_setMetadata_addSublayer(stage)
# stage.Save()

# # === Insert a a sublayer into a USD file ===
# create_stage(target_usd_path, prim=None)
# stage = Usd.Stage.Open(target_usd_path)
# # add two sublayers
# example_setMetadata_addSublayer(stage)
# stage.Save()
# # add one new sublayer inbetween the two in position 1
# insert_sublayer(stage, cylinder_usd_file, 1, rel_paths=None)
# stage.Save()
# # print list of sublayers
# sublayers = get_sublayers(stage)
# print(sublayers)

# # === Get a list of the sublayers of a USD file ===
# stage = Usd.Stage.Open(target_usd_path)
# sublayers = get_sublayers(stage)
# print(sublayers)

# # === Open a stage and check for Default Prim, Set if None exists ===
# stage = Usd.Stage.Open(cone_usd_file)
# example_get_set_defaultPrim(stage)
# stage.Save()

# # === Check for Geo ===
# stage = Usd.Stage.Open(suzanne_usd_file)
# for prim in stage.Traverse():
#     if prim_is_geometry(prim):
#         print("Found geometry prim:", prim.GetPath())
#     if prim_is_xform(prim):
#         print("Found xform prim:", prim.GetPath())

# # === Create stage with two referenced layers ===
# create_stage(target_usd_path)
# stage = Usd.Stage.Open(target_usd_path)
# example_add_references(stage)
# stage.Save()

# # === Set Prim Kind ===
# stage = Usd.Stage.Open(target_usd_path)
# set_prim_kind(stage, "/set/setname", 'group', recursive=False)
# stage.Save()

# # === Recursive Set Prim Type ===
# stage = Usd.Stage.Open(target_usd_path)
# set_prim_type(stage, "/set/setname", 'Xform', recursive=False)
# stage.Save()

# # === Set Scale Rotation & Translation of Prim ===
# stage = Usd.Stage.Open(target_usd_path)
# scale = [2, 2, 2]
# rotation = [45, 0, 0]
# set_sca_rot_tran(stage, "/set/setname/cube_model", scale, rotation, translation=None)
# stage.Save()

# # === Creating Lights ===
# stage = Usd.Stage.Open(target_usd_path)
# example_creating_lights(stage)
# stage.Save()

# # === Remove a Prim ====
# stage = Usd.Stage.Open(os.path.join(script_path, "output.usda"))
# remove_prim(stage, '/set/setname/cube_model')
# stage.Save()

# # === Search for a Prim based on its Name ===
# stage = Usd.Stage.Open(target_usd_path)
# name = find_prim_by_name(stage, "cube_model")
# print(name)

# # === Rename Prim Option 1 ===
# """
# This method renames only the given prim, but has been replaced
# by rename_prim() below
# """
# stage = Usd.Stage.Open(lookdev_usd_file)
# prim = stage.GetPrimAtPath('/Suzanne/Suzanne')
# set_prim_name(stage, prim, 'SuzanneMesh')
# save_as(stage, target_usd_path)

# # === Rename Prim Option 2 ===
# """
# This method renames only the given prim 
# """
# stage = Usd.Stage.Open(lookdev_usd_file)
# prim = stage.GetPrimAtPath('/Suzanne/Suzanne')
# rename_prim(stage, prim, 'SuzanneMesh')
# save_as(stage, target_usd_path)

# # === Modify Path ===
# stage = Usd.Stage.Open(target_usd_path)
# modify_hierarchy(stage, '/set/setname', '/set/has/newName', True)
# stage.Save()

# # === Get prim kind/type ===
# stage = Usd.Stage.Open(target_usd_path)
# prim = stage.GetPrimAtPath('/set')
# prim_kind = get_prim_kind(stage, '/set')
# prim_type = get_prim_type(stage, prim)
# print(prim_kind, prim_type)

# # === Set Extents ===
# stage = Usd.Stage.Open(plane_usd_file)
# prim = stage.GetPrimAtPath('/plane')
# set_extent(stage, prim)
# stage.Save()

# # == Get Extents ===
# stage = Usd.Stage.Open(plane_usd_file)
# extents = get_extent(stage, '/plane/mesh')
# print(extents)

# # === Get point positions on Mesh ===
# stage = Usd.Stage.Open(plane_usd_file)
# # try to get point positions on a Xform
# ppos = get_point_positions(stage, '/plane', print_values=False)
# # get point positions on a Mesh
# ppos = get_point_positions(stage, '/plane/mesh', print_values=True)
# print (ppos)

# # === Set Point Positions ===
# stage = Usd.Stage.Open(plane_usd_file)
# new_point_positions = [(-10, 0, -10), (0, 0, -10), (10, 0, -10), (-10, 0, 0), (0, 0, 0), (10, 0, 0), (-10, 0, 10), (0, 0, 10), (10, 0, 10)]
# set_point_positions(stage, '/plane/mesh', new_point_positions)
# stage.Save()

# # === Create USD Preview Material ===
# """ 
# Here we create a new usd stage file that contains a material
# since this is the first time we create this stage we set the /mtl xform to type scope
# """
# create_stage(material_usd_path, prim=None)
# stage = Usd.Stage.Open(material_usd_path)
# create_usdpreviewsurface(stage, '/mtl/red_shader', diffuseColor=(0.5, 0, 0), specular=0.5)
# set_prim_type(stage, '/mtl', 'Scope', recursive=False)
# stage.Save()

# # === Material Binding to Object ===
# """ 
# The example file lookdev_usd_file already contains a material binding,
# here we create a new blue shader and assign it to the Suzanne mesh.
# """ 
# stage = Usd.Stage.Open(lookdev_usd_file)
# create_usdpreviewsurface(stage, '/_materials/blue_shader', diffuseColor=(0.0, 0.0, 0.5), specular=0.5)
# set_relationship(stage, '/Suzanne/Suzanne', '/_materials/blue_shader', 'material:binding')
# save_as(stage, target_usd_path)

# # === Split out a prim to a new file ===
# stage = Usd.Stage.Open(lookdev_usd_file)
# extract_materials(stage)

# # === Create GeomSubset ===
# stage = Usd.Stage.Open(lookdev_usd_file)
# eye_points = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
# 21, 22, 23, 24, 25, 26, 27, 28,29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41,
# 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63]
# create_geomsubset(stage, '/Suzanne/Suzanne', 'eyes', eye_points)
# save_as(stage, target_usd_path)

# # === Create Over at Prim Path ===
# stage = Usd.Stage.CreateInMemory()
# # create a over only for ../test
# create_over(stage, '/cube_model/cube/test')
# # creat overs for all levels of the prim_path
# create_over(stage, '/cube_model2/cube2/deeper/shape', recursive=True)
# save_as(stage, target_usd_path)

# # === Get Frame Length of Prim in USD ===
# stage = Usd.Stage.Open(anim_usd_file)
# frames = get_frame_count(stage, '/Suzanne_Model')
# print(frames)

# stage = Usd.Stage.Open(cone_usd_file)
# frames = get_frame_count(stage, '/cone_model')
# print(frames)

# # === Add Files to New USD file as Variants ===
# stage = Usd.Stage.CreateNew(target_usd_file)
# variant_files = [cube_usd_file, sphere_usd_file]
# prim_path = "/variantAsset"
# create_variant_file(stage, prim_path, 'geoVar', variant_files, rel_paths=True)
# stage.Save()

# # === Get Variants ===
# stage = Usd.Stage.Open(target_usd_file)
# prim = "/variantAsset"
# vars = get_variants(stage, prim)
# print(vars)
# for key, values in vars.items():
#     print(f"{key}: {', '.join(values)}")

# === Add Variant Set & Variants ===
# stage = Usd.Stage.CreateInMemory()